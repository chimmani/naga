package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AddBankInfo extends vcard{

	

	public static Response addBankInfo( int row,String  bearerToken ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
						//String bearerToken = TestData.getCellData(sheetName, "Token", row);
					//	String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
			////			String ifsc = TestData.getCellData(sheetName, "ifscCode", row);
						String an = TestData.getCellData(sheetName, "accountHolderName", row);
				////		String anum = TestData.getCellData(sheetName, "accountNbr", row);
						String br = TestData.getCellData(sheetName, "bankReferenceId", row);


						String ifsc = ifscCode;
						String anum = lenderAccountNumber;
						String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"ifscCode\":\""+ifsc+"\",\"accountHolderName\":\""+an+"\",\"accountNbr\":\""+anum+"\",\"bankReferenceId\":\""+br+"\"}";
						
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}


	public static void addBankInfo_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

	//	test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				 
				 String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				 
				 
			 }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>addBankInfo_validate FAIL DUE TO</b>"+message); 
			 }
			
			
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>addBankInfo_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>addBankInfo_validate FAIL DUE TO</b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "******************************************************** ");
	}
	
	
}