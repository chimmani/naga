package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class LoanTransactions extends vcard{
	
	public static Response loanTransactions( int row, String bearerToken ) throws Exception{

	String method= new Exception() .getStackTrace()[0].getMethodName();
	test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
	
	
		String sheetName=method;
		int lastrow=TestData.getLastRow(method);
				
		//String bearerToken = TestData.getCellData(sheetName, "Token", row);
		String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);			
		String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
		String tranType = TestData.getCellData(sheetName, "tranType", row);
		String fromDate = TestData.getCellData(sheetName, "fromDate", row);
		String toDate = TestData.getCellData(sheetName, "toDate", row);
		String minAmount = TestData.getCellData(sheetName, "minAmount", row);
		String maxAmount = TestData.getCellData(sheetName, "maxAmount", row);
		String merchantName = TestData.getCellData(sheetName, "merchantName", row);
	
				
		String payload="{\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"tranType\":\""+tranType+"\",\"fromDate\":\""+fromDate+"\",\"toDate\":\""+toDate+"\",\"minAmount\":\""+minAmount+"\",\"maxAmount\":\""+maxAmount+"\",\"merchantName\":\""+merchantName+"\"}";			
					
						Response res=
							given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
							  .when().body(payload).post(baseuri+method).then().extract().response();
					
					
					
		
			return res;

}
	public static void loanTransactions_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>loanTransactions_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>loanTransactions_validate FAIL</b>");
				
			Assert.assertTrue(false);
			
		}
	}
}
