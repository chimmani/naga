package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AddCustomerInfo  extends vcard{
	public static Response addCustomerInfo( int row, String  bearerToken) throws Exception{
		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
			

					//String bearerToken = TestData.getCellData(sheetName, "Token", row);
						//String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
					//	String lenderAppNumber = TestData.getCellData(sheetName, "lenderAppNumber", row);
					//	String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
					//	String lenderRelationshipNumber = TestData.getCellData(sheetName, "lenderRelationshipNumber", row);
						String vdf1 = TestData.getCellData(sheetName, "vdf1", row);
						String vdf2 = TestData.getCellData(sheetName, "vdf2", row);
						String vdf3 = TestData.getCellData(sheetName, "vdf3", row);
						String vdf4 = TestData.getCellData(sheetName, "vdf4", row);
						String vdf5 = TestData.getCellData(sheetName, "vdf5", row);
						String firstName = TestData.getCellData(sheetName, "firstName", row);
						String middleName = TestData.getCellData(sheetName, "middleName", row);
						String lastName = TestData.getCellData(sheetName, "lastName", row);
						String gender = TestData.getCellData(sheetName, "gender", row);
						//String dateOfBirth = TestData.getCellData(sheetName, "dateOfBirth", row);
						String nationality = TestData.getCellData(sheetName, "nationality", row);
						String educationQualification = TestData.getCellData(sheetName, "educationQualification", row);
						String maritalStatus = TestData.getCellData(sheetName, "maritalStatus", row);
						//String phoneNbr = TestData.getCellData(sheetName, "phoneNbr", row);
						//String emailId = TestData.getCellData(sheetName, "emailId", row);
						String fatherName = TestData.getCellData(sheetName, "fatherName", row);
						String motherName = TestData.getCellData(sheetName, "motherName", row);
						String spouseName = TestData.getCellData(sheetName, "spouseName", row);
						String idProofType = TestData.getCellData(sheetName, "idProofType", row);
					//	String idNumber = TestData.getCellData(sheetName, "idNumber", row);
						String addressLine1 = TestData.getCellData(sheetName, "addressLine1", row);
						String addressLine2 = TestData.getCellData(sheetName, "addressLine2", row);
						String city = TestData.getCellData(sheetName, "city", row);
						String state = TestData.getCellData(sheetName, "state", row);
						String zipcode = TestData.getCellData(sheetName, "zipcode", row);
						String residenceType = TestData.getCellData(sheetName, "residenceType", row);
						String addressProofType = TestData.getCellData(sheetName, "addressProofType", row);
						String monthsAtResidence = TestData.getCellData(sheetName, "monthsAtResidence", row);
						String PaddressLine1 = TestData.getCellData(sheetName, "PaddressLine1", row);
						String PaddressLine2 = TestData.getCellData(sheetName, "PaddressLine2", row);
						String Pcity = TestData.getCellData(sheetName, "Pcity", row);
						String Pstate = TestData.getCellData(sheetName, "Pstate", row);
						String Pzipcode = TestData.getCellData(sheetName, "Pzipcode", row);
						String employerName = TestData.getCellData(sheetName, "employerName", row);
						String employerType = TestData.getCellData(sheetName, "employerType", row);
						String designation = TestData.getCellData(sheetName, "designation", row);
						String officeEmailId = TestData.getCellData(sheetName, "officeEmailId", row);
						String monthsAtCurrentJob = TestData.getCellData(sheetName, "monthsAtCurrentJob", row);
						String totalJobExperience = TestData.getCellData(sheetName, "totalJobExperience", row);
						String EaddressLine1 = TestData.getCellData(sheetName, "EaddressLine1", row);
						String EaddressLine2 = TestData.getCellData(sheetName, "EaddressLine2", row);
						String Ecity = TestData.getCellData(sheetName, "Ecity", row);
						String Estate = TestData.getCellData(sheetName, "Estate", row);
						String Ezipcode = TestData.getCellData(sheetName, "Ezipcode", row);
						String incomeType = TestData.getCellData(sheetName, "incomeType", row);
					//	String netMonthlyIncome = TestData.getCellData(sheetName, "netMonthlyIncome", row);
						String nextPayDate = TestData.getCellData(sheetName, "nextPayDate", row);
	
						
						
						
						String payload="{\"partnerAccountDetails\":{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAppNumber\":\""+lenderAppNumber+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"lenderRelationshipNumber\":\""+lenderRelationshipNumber+"\",\n		\"vdf1\": \""+vdf1+"\",\n	\"vdf2\":\""+vdf2+"\",\"vdf3\":\""+vdf3+"\",\"vdf4\":\""+vdf4+"\",\"vdf5\":\""+vdf5+"\"},\"customerInfo\":{\"customerCommonInfo\":{\"firstName\":\""+firstName+"\",\"middleName\":\""+middleName+"\",\"lastName\":\""+lastName+"\",\"gender\":\""+gender+"\",\"dateOfBirth\":\""+dateOfBirth+"\",\"nationality\":\""+nationality+"\",\"educationQualification\":\""+educationQualification+"\",\"maritalStatus\":\""+maritalStatus+"\",\"phoneNbr\":\""+phoneNbr+"\",\"emailId\":\""+emailId+"\",\"fatherName\":\""+fatherName+"\",\"motherName\":\""+motherName+"\",\"spouseName\":\""+spouseName+"\",\"idProofType\":\""+idProofType+"\",\"idNumber\":\""+idNumber+"\"},\n	\"residenceAddressInfo\":{\"addressLine1\":\""+addressLine1+"\",\"addressLine2\":\""+addressLine2+"\",\"city\":\""+city+"\",\"state\":\""+state+"\",\n \"zipcode\":\""+zipcode+"\",\"residenceType\":\""+residenceType+"\",\"addressProofType\":\""+addressProofType+"\","
								+ "		\"monthsAtResidence\": "+monthsAtResidence+"},\n	\"permanentAddressInfo\":{\"addressLine1\":\""+PaddressLine1+"\",\"addressLine2\":\""+PaddressLine2+"\",\"city\":\""+Pcity+"\",\"state\":\""+Pstate+"\",\"zipcode\":\""+Pzipcode+"\"},\n		\"employerInfo\":{\"employerName\":\""+employerName+"\",\"employerType\":\""+employerType+"\",\"designation\":\""+designation+"\",\"officeEmailId\":\""+officeEmailId+"\",\"monthsAtCurrentJob\":\""+monthsAtCurrentJob+"\",\"totalJobExperience\":\""+totalJobExperience+"\"},\n \"employerAddressInfo\":{\"addressLine1\":\""+EaddressLine1+"\",\"addressLine2\":\""+EaddressLine2+"\",\"city\":\""+Ecity+"\",\"state\":\""+Estate+"\",\"zipcode\":\""+Ezipcode+"\"},\"incomeInfo\":{\"incomeType\":\""+incomeType+"\",\"netMonthlyIncome\":\""+netMonthlyIncome+"\",\"nextPayDate\":\""+nextPayDate+"\"}}}";
						
					
				
					//	test.log(LogStatus.INFO, "payload BODY IS**************************************************** "+payload);
									
						Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;
	
	}
	
	

	public static void addCustomerInfo_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		//test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
					int customerId = newData.get("customerId");
					 test.log(LogStatus.INFO, "RESPONSE of customerId: "+customerId);	
					 
					 String status = newData.get("status");
					 test.log(LogStatus.INFO, "RESPONSE of status: "+status);	
				 
			 }else{
				 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>addCustomerInfo_validate FAIL DUE TO </b>"+message);
					
			 	}
			 
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>addCustomerInfo_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>addCustomerInfo_validate FAIL DUE TO </b>"+i);
				
			Assert.assertTrue(false);
			
		}
		test.log(LogStatus.INFO, "******************************************************** ");
	}
	
	
}
