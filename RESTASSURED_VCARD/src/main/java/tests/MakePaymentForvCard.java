package tests;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class MakePaymentForvCard extends vcard{

	public static Response makePaymentForvCard( int row, String bearerToken ) throws Exception{

		String method= new Exception() .getStackTrace()[0].getMethodName();
		test.log(LogStatus.PASS, "<FONT color=green style=Arial> ********Performing  API ********"+method);
		
		
			String sheetName=method;
			int lastrow=TestData.getLastRow(method);
					
			//String bearerToken = TestData.getCellData(sheetName, "Token", row);
		//	String vCardCustomerCode = TestData.getCellData(sheetName, "vCardCustomerCode", row);
		//	String lenderAccountNumber = TestData.getCellData(sheetName, "lenderAccountNumber", row);
			String paymentType = TestData.getCellData(sheetName, "paymentType", row);
			String paymentAmt = TestData.getCellData(sheetName, "paymentAmt", row);
			String isPresentment = TestData.getCellData(sheetName, "isPresentment", row);
			String paymentPostDate = TestData.getCellData(sheetName, "paymentPostDate", row);
			String paymentCategory = TestData.getCellData(sheetName, "paymentCategory", row);
			String paymentReferenceNumber = TestData.getCellData(sheetName, "paymentReferenceNumber", row);
			String paymentMode = TestData.getCellData(sheetName, "paymentMode", row);
			String vCardTranId = TestData.getCellData(sheetName, "vCardTranId", row);
			String merchantTxnId = TestData.getCellData(sheetName, "merchantTxnId", row);
			String requestedTimeStamp = TestData.getCellData(sheetName, "requestedTimeStamp", row);
			String remarks = TestData.getCellData(sheetName, "remarks", row);
			String transactionTimeStamp = TestData.getCellData(sheetName, "transactionTimeStamp", row);
			String tranStatus = TestData.getCellData(sheetName, "tranStatus", row);
			String payerVpa = TestData.getCellData(sheetName, "payerVpa", row);
			String payerName = TestData.getCellData(sheetName, "payerName", row);
			String upiRefNum = TestData.getCellData(sheetName, "upiRefNum", row);
			String pgAuthCode = TestData.getCellData(sheetName, "pgAuthCode", row);
			String pgTranRefNum = TestData.getCellData(sheetName, "pgTranRefNum", row);
			String pgBankRefNum = TestData.getCellData(sheetName, "pgBankRefNum", row);
			
			
			
						
			String payload="{\"vCardCustomerCode\":\""+vCardCustomerCode+"\",\"lenderAccountNumber\":\""+lenderAccountNumber+"\",\"paymentType\":\""+paymentType+"\",\"paymentAmt\":"+paymentAmt+",\"isPresentment\":\""+isPresentment+"\",\"paymentPostDate\":\""+paymentPostDate+"\",\"paymentCategory\":\""+paymentCategory+"\",\"paymentReferenceNumber\":\""+paymentReferenceNumber+"\",\"paymentMode\":\""+paymentMode+"\",\"vCardTranId\":\""+vCardTranId+"\",\"merchantTxnId\":\""+merchantTxnId+"\",\"requestedTimeStamp\":\""+requestedTimeStamp+"\",\"remarks\":\""+remarks+"\",\"transactionTimeStamp\":\""+transactionTimeStamp+"\",\"tranStatus\":\""+tranStatus+"\",\"payerVpa\":\""+payerVpa+"\",\"payerName\":\""+payerName+"\",\"upiRefNum\":\""+upiRefNum+"\",\"pgAuthCode\":\""+pgAuthCode+"\",\"pgTranRefNum\":\""+pgTranRefNum+"\",\"pgBankRefNum\":\""+pgBankRefNum+"\"}";			
						
							Response res=
								given().headers("Authorization",bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
								  .when().body(payload).post(baseuri+method).then().extract().response();
						
						
						
			
				return res;

	}


	public static void makePaymentForvCard_validate(Response res4) 	 
	{
		String b4=	res4.getBody().asString();

		//test.log(LogStatus.INFO, "RESPONSE BODY IS**************************************************** "+b4);
		
		String i=String.valueOf(res4.getStatusCode());
	
		
		if(i.equals("200")){
			 JsonPath newData = res4.jsonPath();
			 	String message = newData.get("messages[0].description");
			 test.log(LogStatus.INFO, "RESPONSE of Message: "+message);	
			 	
			 if(message.contains("successfully")){
				  String status = newData.get("status");
				 test.log(LogStatus.INFO, "RESPONSE of status: "+status);
				
				 
				 int loanTranCode = newData.get("loanTranCode");
				 test.log(LogStatus.INFO, "RESPONSE of loanTranCode: "+loanTranCode);
				 
				 
			 
			  }else{
				  test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>makePaymentForvCard_validate FAIL DUE TO</b>"+message);
					
			 }
			
			
			
			
			
			
			 test.log(LogStatus.PASS, "<FONT color=green> <b style='text-transform:uppercase;font-weight:bold'>makePaymentForvCard_validate PASS</b>");
				
		}else{
			 test.log(LogStatus.FAIL, "<FONT color=red> <b style='text-transform:uppercase;font-weight:bold'>makePaymentForvCard_validate FAIL DUE TO</b>"+i);
				
			Assert.assertTrue(false);
			
		}

		test.log(LogStatus.INFO, "**************************************************************************************************************** ");
		
	}

}
